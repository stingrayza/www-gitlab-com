---
layout: handbook-page-toc
title: "Customer Segments & Associated Metrics"
---

TAM customer segment information has moved to a new location within the TAM handbook.

➡️ [**TAM Segments**](../segment/)
