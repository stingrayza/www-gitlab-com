---
layout: handbook-page-toc
title: Transitioning Accounts from Pre-Sales to Post-Sales
description: >-
  How to effectively transition a customer from the pre-sales engagement to post-sales to ensure the customer is successful.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a member of the pre-sales team, closing another successful deal for GitLab, it is important to pave the way for the TAM. A successful TAM introduction is a critical step in the customer lifecycle. An effective TAM introduction will lead to customer excitement about the product and increase the TAM's credibility as they assist the customer through onboarding, implementation, and adoption. 

<!-- NOTE: This course probably needs to be checked to ensure it's not conflicting with the new guidelines -->
Please review this course on [Introducing the Technical Account Manager](https://levelup.gitlab.com/courses/introducing-the-technical-account-manager). 

## Transition Quick Summary

| | Commercial | Enterprise | 
| ---  | ---   | ---   |
| **Who** coordinates the TAM introduction? | Account Executive | Solutions Architect | 
| **When** does the introduction happen? | `Closed Won` | `3-Technical Evaluation` or `4-Proposal` |
| **Why** does the introduction happen at this stage? | Since assignments are multifaceted, a TAM/CSM manager typically needs to make the assignment. This is triggered automatically on `Closed Won`, however prior to that the account team can [request a TAM via an issue](https://gitlab.com/gitlab-com/customer-success/tam-triage-boards/commercial/-/issues/new) . | Introduce the TAM when business value discussions are still being had within the account, allowing them to build relationships with the key strategic decision-makers and help the customer visualize their implementation journey. This sets the TAM up for continued engagement at the right level within the account. |
| **How** does the introduction get scheduled? | SA/AE will send an introduction email to the customer team or schedule a "Welcome to GitLab" call. | SA will schedule a "Welcome to GitLab" call with the customer team, after having synced internally with the full [account team](/handbook/customer-success/account-team/) (inc. TAM) | 
| **Where** can the TAM access account information? | - Custom Pitch Deck <br> - Command Plan <br> - POV (when applicable) | - Customer Meeting Notes Doc <br> - Command Plan <br> - Account Plan | 

## Enterprise TAM Transition Process

In enterprise deals we want to introduce the TAM to the customer while they are still a prospect nearing the end of [stage](/handbook/sales/field-operations/gtm-resources/) `3-Technical Evaluation` or when transitioning to `4-Proposal`.

### Why do we begin the transition prior to the sale?

This is a great time to introduce the TAM to the decision-makers and key stakeholders, while the SA still has regular contact and a close working relationship. These business counterparts aren't usually involved in future cadence calls, so fostering that relationship early helps TAMs to engage with the stakeholders in the business that are best positioned to help drive adoption, build a successful goals-oriented partnership and ultimately, lead to improved success planning, EBRs, etc.

Additionally, TAM engagement can be requested earlier in [stage](/handbook/sales/field-operations/gtm-resources/) `3-Technical Evaluation` (but not before) in the following situations:

  - If a POV has been conducted - Upon POV completion & results presentation meeting where the next steps agreed upon

<!--

NOTE: I'm not convinced these are valid reasons to engage a TAM prior to stage 3, which is what we are now talking about...

  - A shared customer issue tracking project has been created that will affect the account long-term
  - As requested by the SA if the TAM has a specific subject matter expertise relevant to the conversation
  -->

### How does the SA engage with the TAM?

In order to ensure the TAM workload is distributed effectively, SAs should reach out to the relevant TAM Manager in their region to request a TAM to be assigned to the account. As part of this process, SAs should provide at least the following information:

- Salesforce link to the Account
- Salesforce link to the Opportunity
- Expected Close Date
- [LAM](https://docs.google.com/document/d/10hWaaCeeTY1zcZyLP8SkMZhpjv_tm7GHF049c9uD-GQ/edit#bookmark=id.4pg1rkj18ebn) of the Account

After a TAM is assigned by the manager, the SA should arrange an internal [account team](/handbook/customer-success/account-team/) meeting.

### Internal Account Team Meeting

In preparation for introducing the TAM, the SA should schedule a meeting with the entire [account team](/handbook/customer-success/account-team/) to discuss general information about the customer, to include:

- Account Information
  - Name of organisation
  - Industry / sector they operate in
  - Any recent news articles about them, or the sector
- Key people
  - Champion
  - Decision maker / economic buyer
  - Main points of contact
- Command Plan and reasons for purchasing GitLab
  - Defined business outcomes
  - Pain points
  - Metrics
  - Impediments to adoption and known risks
- Services engagement
  - Scope and deliverables
  - Timeline
- POV overview (if a POV was conducted)
  - Technical objectives and results
  - Pending items to resolve
  - Customer's required capabilities
  - Overall sentiment coming out of the POV
- Open Support Tickets

If the Solutions Architect has created a collaboration project, the [account team](/handbook/customer-success/account-team/) should review it together, and ensure that `readme.md` details and issues are up to date.

### TAM introduction to the customer

Following the internal account team meeting, the SA should set up a TAM introduction meeting with the customer. When arranging this meeting, the SA can provide the customer with a high-level overview of the TAM role, and what the TAM engagement _might_ look like going forward. However, the SA (and SAL) should refrain from making any statements on behalf of the TAM, as to what activities they _will_ do.

Prior to the customer meeting, the TAM should work with the SA to prepare the [TAM program overview deck](https://docs.google.com/presentation/d/1n_Tex7gm8_UgxEaUy8YR3wccb73bsWOugdQ9mQX_oMU/edit?usp=sharing) to be presented as part of the meeting. This will be starting point of the TAM's relationship with the customer, and it's important to convey what the TAM's role is, and the expectations from both sides moving forward. As this one agenda item of the SA's meeting with the customer, the TAM portion should not be extensive and should be used to set the stage for future conversations.

While the opportunity progresses through commercial discussions, the TAM should engage with the SA where appropriate, in order to maintain the relationships with the strategic personnel and decision makers within the organisation.

### Shifting responsibility from SAL & SA to TAM

Once the opportunity moves [stage](/handbook/sales/field-operations/gtm-resources/) to `Closed Won` the TAM should schedule the kickoff meeting with the customer to go into more detail about the customer's objectives and the TAM program. Please [review the details for the TAM kickoff call](/handbook/customer-success/tam/onboarding/#kickoff-call) to learn more about how to conduct this meeting.

Now that the account is in post-sales, the TAM takes primary responsibility for guidance & best practices conversations, customer enablement, and product usage. Most responsibilities of the SAL & SA will transition to the TAM at this point.

During customer onboarding, the SA may choose to stay engaged with the customer to help facilitate a seamless introduction and address any ongoing activities from the POV. This should be done in such a way as to allow the TAM to take over those activities, so all conversations with the customer on these items should include the TAM.

Once the account is fully transitioned to post-sales, the SA may be invited by the TAM to [re-engage under certain circumstances](#re-engaging-the-salae-and-sa).

## Commercial TAM Transition Process

A successful transition happens when a customer does not have to repeat themselves in regards to their technical environment and goals they have in mind with leveraging GitLab.

In commercial deals the TAM ought to be introduced at time of assignment which occurs after the deal moves to `Closed Won`.  For opportunities where the TAM assignment is known or there has been an in-depth technical evaluation, it is encouraged to engage the TAM team earlier in the sales life cycle so that the technical requirements can be seamlessly brought over from the pre-sales and into the post-sales technical motions.

The collective account team (AE / SA (when assigned) / TAM) should organize an internal sync to properly handover any technical details of the customer. The goal of this meeting is to give the TAM enough context to engage with the customer without them having to re-hash their current state and near-term desired state with GitLab. 

<!-- NOTE: Is there an Internal Account Team Meeting expected in commercial handover? -->

### TAM introduction to the customer

At the conclusion of the pre-sales engagement, the Account Executive should set up an introduction to the TAM, provide the customer an overview of the TAM role, position the TAM as the primary success manager to help the customer achieve their positive business outcomes as identified in the Command Plan.

The AE/SA will need to ensure that Command Plan and Pitch Deck (R7) are complete before scheduling an internal transition meeting to brief the TAM. AE/SA will then introduce the TAM via email. 

From this initial introduction, the TAM should schedule the kickoff meeting with the customer to go into more detail about the customer's objectives and the TAM program. Please [review the details for the TAM kickoff call](/handbook/customer-success/tam/onboarding/#kickoff-call) to learn more about how to conduct this meeting.

### Shifting responsibility from AE & SA to TAM

Once the account is in post-sales, the TAM takes primary responsibility for guidance & best practices conversations, customer enablement, and product usage. Most responsibilities of the AE & SA will transition to the TAM at this point.

During customer onboarding, the SA may choose to stay engaged with the customer to help facilitate a seamless introduction and address any ongoing activities from the POV. This should be done in such a way as to allow the TAM to take over those activities, so all conversations with the customer on these items should include the TAM.

Once the account is fully transitioned to post-sales, the SA may be invited by the TAM for the following activities:

- POV for an additional business unit or a license tier upgrade
- Professional Services scoping and discovery for a [Statement of Work](/handbook/customer-success/professional-services-engineering/working-with/#custom-services-sow-creation-and-approval)
- Support of Lunch & Learns, GitLab Days, and on-site evangelism opportunities
- Executive Business Reviews (EBRs)

The SA may also be involved, as needed and at the discretion of the SA & their manager, to support customer enablement, EBR delivery, product demo, or other customer-facing activities for which the TAM may ask for assistance.

## Re-Engaging the SAL/AE and SA

Common to both Enterprise and Commercial activity, there are instances where the TAM will need to reengage the SAL/AE and SA. Here are a few examples of customer requests the TAM can listen for in order to reengage them: 

  - Development of expansion plans with the SAL and TAM leading to tier upgrades
  - A POV, product evaluation, RFP, or security begins for a "new" single team or an enterprise-wide tier upgrade not for renewal purposes
  - SAs may assist TAMs with an existing customer team on an exception basis. SA leadership approval is required.
    - Customer enablement assistance when specifically requested by the TAM team
    - Workshops assistance when specifically requested by the TAM team
    - ROI analysis for an existing customer team when requested by the TAM team
    - Unless it's for a tier upgrade, presales activities such as POVs and RFPs should be avoided for an existing customer team. Like with any presales opportunity, these activities should not be offered as a sales tools, but leveraged when a prospect/customer requires these activities as part of their evaluation.
  - Any Professional Services that are being discussed and may require an SOW
  - If a TAM is over-committed or unable to support a customer request, the SA may be requested to assist
  - Support of GitLab days or other on-site evangelism of GitLab at customer sites
  - Executive Business Reviews (EBRs)

## Useful Links

  [us-public-sector](https://gitlab.com/gitlab-com/us-public-sector) parent project for all public sector work

  [account-management](https://gitlab.com/gitlab-com/account-management/) parent project for account-specific work and collaboration with the rest of the sales

  [customer-success](https://gitlab.com/gitlab-com/customer-success) parent project for Customer Success employee, shared or triage work

  


